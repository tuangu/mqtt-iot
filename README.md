## This repository contains code for Internet of Things project

It contains three components namely hardware, frontend, and server.

The hardware code is written for [Particle Photon](https://store.particle.io/collections/photon). You will need this and an accelerometer to test the code.

![hardware wiring](https://i.imgur.com/QuUV2aK.png)

The frontend and server code can be run locally or deloyed on online service like Heroku.

## Documentation

The documentation for this project can be found here: [Report](https://drive.google.com/file/d/1ViPL5VgtKNBnWYzGeSxRMNYBzvfxgwBR/view?usp=sharing)