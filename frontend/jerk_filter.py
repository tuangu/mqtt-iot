import numpy as np

class JerkFilter:
    """
    Jerk Filter
    """

    def __init__(self, capacity=200):
        """
        _data has shape of len x 3
        """
        self._data = []
        self._filtered_data = []
        self._max_capacity = capacity

    def reset(self):
        self._data = []
        self._filtered_data = []

    def get_filter_data(self, accel_data):
        """
        Return filtered data
        accel_data has shape of 1 x 3
        """

        self._data.append(accel_data)

        if len(self._data) < 2:
            return None
        else:
            alpha = self.angle_of_direction(np.array(self._data[-2]), np.array(self._data[-1]))
            mag, sign, change = self.magnitude_of_direction(np.array(self._data[-2]), np.array(self._data[-1]))
            jerk = self.jerk_filter(alpha, mag, sign)
            self._filtered_data.append(jerk)

        if len(self._data) > self._max_capacity:
            self._data = self._data[1:]
            self._filtered_data = self._filtered_data[1:]

        return np.array(self._filtered_data)


    def unit_vector(self, vector):
        """ Returns the unit vector of the vector.  """
        return vector / np.linalg.norm(vector)

    def angle_between(self, v1, v2):
        """ Returns the angle in radians between vectors 'v1' and 'v2'::

                >>> angle_between((1, 0, 0), (0, 1, 0))
                1.5707963267948966
                >>> angle_between((1, 0, 0), (1, 0, 0))
                0.0
                >>> angle_between((1, 0, 0), (-1, 0, 0))
                3.141592653589793
        """
        v1_u = self.unit_vector(v1)
        v2_u = self.unit_vector(v2)
        return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

    def angle_of_direction(self, accel_data_1, accel_data_2):
        """
        Return the angle of the direction change in degree.
        accel_data_i has shape 1 x 3
        """

        rad_to_deg = 57.29578

        angle_diff = self.angle_between(accel_data_1, accel_data_2) * rad_to_deg

        return angle_diff

    def magnitude_of_direction(self, accel_data_1, accel_data_2):
        """
        Return the change of the magnitude of acceleration
        accel_data_i are numpy array and has shape 1 x 3
        """
        if np.linalg.norm(accel_data_2) > np.linalg.norm(accel_data_1):
            sign = 1
        else:
            sign = -1

        change = accel_data_2 - accel_data_1
        mag_change = np.asscalar(np.linalg.norm(change, keepdims=True))

        return mag_change, sign, change

    def jerk_filter(self, alpha, magnitude, sign):
        """
        alpha, magnitude, sign: scalar
        """

        return (1 + alpha / 180.0) * magnitude * sign