import numpy as np 
from scipy.integrate import cumtrapz

class Speed():
    def __init__(self):
        self._max_speed = 0
        self._accel_x = []
        self._accel_y = []
        self._accel_z = []
        self._counter = 0
        
    def put_data(self, accel_data):
        self._accel_x.append(accel_data[0] * 9.81)
        self._accel_y.append(accel_data[1] * 9.81 - 9.81)
        self._accel_z.append(accel_data[2] * 9.81)
        self._counter += 1

    def get_velocity(self):
        
        self._x_mean = np.mean(self._accel_x)
        self._y_mean = np.mean(self._accel_y)
        self._z_mean = np.mean(self._accel_z)

        self._x_velocity = cumtrapz(self._accel_x - self._x_mean, dx=0.01)
        self._y_velocity = cumtrapz(self._accel_y - self._y_mean, dx=0.01)
        self._z_velocity = cumtrapz(self._accel_z - self._z_mean, dx=0.01)
        
        self._velocity = []
        for i in range(self._counter - 1):
            self._velocity.append(np.sqrt(np.square(self._x_velocity[i]) + np.square(self._y_velocity[i]) + np.square(self._z_velocity[i])))

        return self._velocity
    
    def get_max_speed(self):
        self.get_velocity()
        self._max_speed = np.maximum(self._max_speed, np.max(self._velocity))

        return self._max_speed