import threading
import queue
import paho.mqtt.client as mqtt
import asyncio
import websockets
import numpy as np
import json

from kick_detection import has_kick
from jerk_filter import JerkFilter
from speed import Speed

jerk = JerkFilter(capacity=300)
speed_data = Speed()

ws_plotter_type = "plotter"
ws_add_to_kick_counter = '{"type":"addKickCount"}'
ws_max_speed = '{"type": "maxSpeed", "speed": '
data_queue = queue.Queue()

# mayby another queue for BLE

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code ", str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("team16/test/accelerometer")
	
	# mayby another subscriber for BLE

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    sensor_data = msg.payload.decode('ascii')
    # sensor_data = eval(sensor_data)
    # sensor_data = json.loads(sensor_data)
    userdata['data_queue'].put(sensor_data)
    # print('{},{},{},{}'.format(sensor_data['timestamp'], sensor_data['accel_x'], sensor_data['accel_y'], sensor_data['accel_z']))

def main():
	client.loop_forever()
	
	
def mqtt_main(data_queue):
    print('mqtt_main started')
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.user_data_set(userdata={'data_queue': data_queue})
    client.username_pw_set('team16', 'nqnqb')
    client.connect("backend.abelonditi.com", 1883, 60)    
    client.loop_start()

# Adds type for ws JSON if needed
def addWStype(data, type):
	data_with_type = data[:1] + '"type":' + '"' +  type + '"' + "," + data[1:]
	return data_with_type

async def webSocketHandler(websocket, path):
    counter = 0
    max_speed = 0

    while True:
        if (data_queue.empty() is False):
            data = data_queue.get()
            await websocket.send(addWStype(data , ws_plotter_type))  #send data for plotter

            accel_data = json.loads(data)
            accel_data = [accel_data['accel_x'], accel_data['accel_y'], accel_data['accel_z']]
            filtered_data = jerk.get_filter_data(accel_data)
            speed_data.put_data(accel_data)

            if filtered_data is not None:
                kick_count = has_kick(filtered_data, threshold=2)
            else:
                continue

            if kick_count > 0:
                print("Kick detected")
                jerk.reset()
                await websocket.send(ws_add_to_kick_counter)
            			
            if (counter > 100) and (max_speed < 20):
                counter = 0
                curr_max_speed = speed_data.get_max_speed()
                if curr_max_speed > max_speed:
                    max_speed = curr_max_speed
                    speed_str = '{"type": "maxSpeed", "speed": %.2f}' % (max_speed)
                    await websocket.send(speed_str)
            else:
                counter += 1
			
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set('team16', 'nqnqb')
client.connect("backend.abelonditi.com", 1883, 60)

start_server = websockets.serve(webSocketHandler, '127.0.0.1', 5678)
	
mqtt_thread = threading.Thread(target=mqtt_main, args=(data_queue, ))
mqtt_thread.start()

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

