import numpy as np
from jerk_filter import *

def has_kick(jerk, threshold=100, sampling_rate=100, delta=2):
    """
    Count number of kick
    jerk: filtered data using jerk filter
    threshold: diff between 2 samples which is considered as a leg swing
    delta: the number of seconds it takes to kick
    """
    
    len = jerk.shape[0]
    threshold = np.abs(threshold)
    sample_per_kick = sampling_rate  * delta
    kicks = 0
    
    back_swing = False
    last_back_swing = 0
    
    for i in range(1, len):
        diff = np.abs((jerk[i] - jerk[i-1]))
        
        if diff > threshold:
            
            if last_back_swing == 0:
                last_back_swing = i
                continue
            
            index_diff = i - last_back_swing
            
            if (index_diff > 10) and (index_diff < sample_per_kick):
                kicks += 1
                last_back_swing = 0
            else:
                last_back_swing = i
            
    return kicks